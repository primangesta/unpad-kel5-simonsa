<!-- BEGIN: Header-->
<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu navbar-static-top bg-primary navbar-brand-center">
	<div class="navbar-header d-xl-block d-none" style="left:25px">
		<ul class="nav navbar-nav flex-row">
			<li class="nav-item">
				<a class="navbar-brand" href="#">
					<h2 class="brand-text mb-0">Monitoring & Optimalisasi Manajemen Sampah UNPAD</h2>
				</a>
			</li>
		</ul>
	</div>
	<div class="navbar-wrapper">
		<div class="navbar-container content">
			<div class="navbar-collapse" id="navbar-mobile">
				<div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
					<ul class="nav navbar-nav">
						<li class="nav-item mobile-menu mr-auto"><a class="nav-link nav-menu-main menu-toggle" href="javascript:void(0);"><i class="bx bx-menu"></i></a></li>
					</ul>
				</div>
				<ul class="nav navbar-nav float-right d-flex align-items-center">
					<li class="nav-item d-none d-lg-block">
						<a class="nav-link nav-link-expand">
							<i class="ficon bx bx-fullscreen"></i>
						</a>
					</li>
					<li class="dropdown dropdown-user nav-item"><a class="dropdown-toggle nav-link dropdown-user-link" href="javascript:void(0);" data-toggle="dropdown">
							<div class="user-nav d-lg-flex d-none"><span class="user-name">{{Auth::user()->nama}}</span><span class="user-status">Online</span></div><span><img class="round" src="{{asset('assets/images/portrait/small/unpad.png')}}" alt="avatar" height="40" width="40"></span>
						</a>
						<div class="dropdown-menu dropdown-menu-right pb-0">
							<a class="dropdown-item" href="{{route('profile')}}"><i class="bx bx-user mr-50"></i> Edit Profile</a>
							<div class="dropdown-divider mb-0"></div><a class="dropdown-item" href="{{route('logout')}}"><i class="bx bx-power-off mr-50"></i> Logout</a>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>
</nav>
<!-- END: Header-->