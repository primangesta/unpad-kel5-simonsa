@extends('layouts.backendMainLayout')
@section('content')
<div class="app-content content">
	<div class="content-overlay"></div>
	<div class="content-wrapper">
		<div class="content-header row">
		</div>
		<div class="content-body">
			<!-- Dashboard Analytics Start -->
			<section id="dashboard-analytics">
				<div class="row">
					<!-- Website Analytics Starts-->
					<div class="col-md-12 col-sm-12">
						<div class="card">
							<div class="card-header d-flex justify-content-between align-items-center">
								<h4 class="card-title">Jumlah Sampah 2021</h4>
								<a href="{{route('download_manajemen_sampah')}}" style="margin-bottom:20px" class="btn btn-primary glow mb-1 mb-sm-0 mr-0 mr-sm-1">Download</a>
							</div>
							<div class="card-body pb-1">
								<div class="d-flex align-items-center flex-wrap">
									<div class="sessions-analytics mr-2">
										<i class="bx bx-trending-up align-middle"></i>
										<span class="align-middle text-muted">Estimasi Keuntungan Penjualan Sampah (Non Organik)</span>
										<div class="d-flex">
											<h3 class="mt-1 ml-50">{{format_uang($data)}}</h3>
										</div>
									</div>
								</div>
								<div id="analytics-bar-chart" class="my-75"></div>
							</div>
							<div class="card-footer">
								<label>*Estimasi harga diambil dari total berat sampah non organik dikali harga termurah/Kg (Rp. 200/Kg)</label>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<!-- Growth Chart Starts-->
					@foreach($result as $item)
					<div class="col-xl-6 col-md-6 col-12 growth-card">
						<div class="card">
							<div class="card-header">
								<h6>Persentase Muatan Tong Sampah</h6>
							</div>
							<div class="card-body text-center">
								<div id="growth-Chart-{{$item['code']}}"></div>
								<h6 class="mt-2">{{$item['nama']}}</h6>
							</div>
						</div>
					</div>
					@endforeach
				</div>
			</section>
			<!-- Dashboard Analytics end -->

		</div>
	</div>
</div>
@endsection

@section('script')
<script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-trendline"></script>
<script>
$(window).on("load", function () {
	var spinHandle = loadingOverlay.activate();
	var $dataSeries = [];
	var $dataPenampung = [];
	$.ajax({
		url: "{{ url('getDataSampah') }}",
		type: 'get',
		dataType: 'json',
		success: function (res) {
			$dataSeries = res;
			loadingOverlay.cancel(spinHandle);
		},
		async: false
	});

	$.ajax({
		url: "{{ url('getDataPenampungan') }}",
		type: 'get',
		dataType: 'json',
		success: function (res) {
			$dataPenampung = res;
			loadingOverlay.cancel(spinHandle);
		},
		async: false
	});

	// Bar Chart
	// ---------
	var $primary = '#5A8DEE';
	var $gray_light = '#828D99';
	var $danger = '#FF5B5C';
	var analyticsBarChartOptions = {
		chart: {
		height: 260,
		type: 'bar',
		toolbar: {
			show: false
		}
		},
		plotOptions: {
		bar: {
			horizontal: false,
			columnWidth: '20%',
			endingShape: 'rounded'
		},
		},
		dataLabels: {
		enabled: false
		},
		colors: [$primary, '#B6CDF8'],
		fill: {
		type: 'gradient',
		gradient: {
			shade: 'light',
			type: "vertical",
			inverseColors: true,
			opacityFrom: 1,
			opacityTo: 1,
			stops: [0, 70, 100]
		},
		},
		series: $dataSeries,
		xaxis: {
		categories: ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Des'],
		axisBorder: {
			show: false
		},
		axisTicks: {
			show: false
		},
		labels: {
			style: {
			colors: $gray_light
			}
		}
		},
		yaxis: {
		min: 0,
		max: 300,
		tickAmount: 3,
		labels: {
			style: {
			color: $gray_light
			}
		}
		},
		legend: {
		show: false
		},
		tooltip: {
		y: {
			formatter: function (val) {
			return val + " Kg"
			}
		}
		}
	}

	var analyticsBarChart = new ApexCharts(
		document.querySelector("#analytics-bar-chart"),
		analyticsBarChartOptions
	);

	analyticsBarChart.render();

	// Growth Radial Chart
	// --------------------
	for (let i = 0; i < $dataPenampung.length; i++) {
		var growthChartOptions = [];
		growthChartOptions[$dataPenampung[i]['code']] = {
			chart: {
				height: 220,
				type: 'radialBar',
				sparkline: {
					show: true
				}
			},
			grid: {
				show: false,
			},
			plotOptions: {
				radialBar: {
					size: 100,
					startAngle: -135,
					endAngle: 135,
					offsetY: 10,
					hollow: {
						size: '60%',
					},
					track: {
						strokeWidth: '90%',
						background: '#fff'
					},
					dataLabels: {
					value: {
						offsetY: -10,
						color: '#475f7b',
						fontSize: '26px'
					},
					name: {
						fontSize: '15px',
						color: "#596778",
						offsetY: 30
					},
					}
				},
			},
			colors: [$danger],
			fill: {
				type: 'gradient',
				gradient: {
					shade: 'dark',
					type: 'horizontal',
					shadeIntensity: 0.5,
					gradientToColors: [$primary],
					inverseColors: true,
					opacityFrom: 1,
					opacityTo: 1,
					stops: [0, 100]
				},
			},
			stroke: {
				dashArray: 3
			},
			series: [$dataPenampung[i]['total']],
			labels: [$dataPenampung[i]['kategori']],
		}

		var growthChart = new ApexCharts(
			document.querySelector("#growth-Chart-"+$dataPenampung[i]['code']),
			growthChartOptions[$dataPenampung[i]['code']]
		);

		growthChart.render();
		console.log($dataPenampung[i]['code']);
	}


});
</script>
@endsection

