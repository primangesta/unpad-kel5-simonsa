@extends('layouts.backendMainLayout')
@section('content')
<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-body">
            <!-- users list start -->
            <section class="users-list-wrapper">
                <div class="users-list-filter px-1">
                    <form action="#" method="GET">
                        <div class="row border rounded py-2 mb-2">
                            <div class="col-12 col-sm-6 col-lg-11">
                                <label for="users-list-verified">Search</label>
                                <fieldset class="form-group">
                                    <input type="text" class="form-control" placeholder="Keyword" name="q" value="{{app('request')->input('q')}}">
                                </fieldset>
                            </div>
                            <div class="col-12 col-sm-6 col-lg-1 d-flex align-items-center" style="padding:0px;">
                                <button type="submit" class="btn btn-icon rounded-circle btn-outline-primary mr-1"><i class="bx bx-search"></i></button>
                                <a href="{{route('kategori_sampah')}}" class="btn btn-icon rounded-circle btn-outline-primary mr-1"><i class="bx bx-trash"></i></a>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="row" id="table-hover-row">
                    <div class="col-12">
                        <div class="card" style="min-height:400px">
                            <div class="card-header">
                                <h4 class="card-title">{{$title}}</h4>
                                <div class="d-flex align-items-center">
                                    <a href="{{route('kategori_sampah_create')}}"><button type="button" class="btn btn-icon rounded-circle btn-outline-primary"><i class="bx bx-plus"></i></button></a>
                                </div>
                            </div>
                            <div class="card-body">
                                @if($message = Session::get('success'))
                                <div class="alert alert-success alert-dismissible mb-2" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <div class="d-flex align-items-center">
                                        <i class="bx bx-like"></i>
                                        <span>
                                            {{$message}}
                                        </span>
                                    </div>
                                </div>
                                @endif
                                <div class="table-responsive">
                                    <table class="table table-hover mb-0">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama</th>
                                                <th>Deskripsi</th>
                                                <th>Tanggal</th>
                                                <th style="min-width:150px">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if(isset($results))
                                                <?php $no = 1; ?>
                                                @foreach($results as $item)
                                                <tr>
                                                    <td>{{$no++}}</td>
                                                    <td>{{$item->nama}}</td>
                                                    <td>{{$item->deskripsi}}</td>
                                                    <td>{{$item->created_at->format('m/d/Y')}}</td>
                                                    <td>
                                                        <a href="{{route('kategori_sampah_edit',$item->id)}}" style="margin-bottom:20px"><i class="badge-circle badge-circle-light-secondary bx bx-pencil font-medium-1"></i></a>
                                                        <form action="{{route('kategori_sampah_delete',$item->id)}}" method="POST" style="display:contents">
                                                            @csrf
                                                            <button type="submit" onclick="return confirm('Anda yakin akan menghapus data ini?');" style="border: none; background: none;"><i class="badge-circle badge-circle-light-secondary bx bx-trash font-medium-1"></i></button>
                                                        </form>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            @else
                                            <tr>
                                                <td colspan="5" style="text-align: center;">No Records</td>
                                            </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @if(isset($results))
                <div class="justify-content-end">
                    {!! $results->links() !!}
                </div>
                @endif

            </section>
            <!-- users list ends -->
        </div>
    </div>
</div>
<!-- END: Content-->
@endsection