@extends('layouts.backendMainLayout')
@section('content')
<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
            <!-- users edit start -->
            <section class="users-edit">
                <div class="card">
                    <div class="card-body">
                        
                        @if (count($errors) > 0)
                            @foreach ($errors->all() as $error)
                                <div class="alert alert-danger alert-dismissible mb-2" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <div class="d-flex align-items-center">
                                        <i class="bx bx-error"></i>
                                        <span>
                                            {{$error}}
                                        </span>
                                    </div>
                                </div>
                            @endforeach
                        @endif

                        <form class="form-validate" action="{{ route('manajemen_sampah_store') }}" method="POST">
                            @csrf
                            <div class="row">
                                
                                <div class="col-12 col-sm-12">
                                    <label>Titik Penampungan</label>                                    
                                    <fieldset class="form-group">
                                        <select class="form-control" id="titikPenampungan" name="id_titik_penampungan" onChange="getCategories()">
                                            <option value="">Pilih Titik Penampungan</option>
                                            @foreach($titikPenampungan as $item)
                                            <option value="{{$item->id}}">{{$item->nama}}</option>
                                            @endforeach
                                        </select>
                                    </fieldset>
                                    <label>Kategori Sampah</label>                                    
                                    <fieldset class="form-group">
                                        <select class="form-control" id="kategoriSampah" name="id_kategori_sampah">
                                            <option value="">Pilih Kategori Sampah</option>
                                            @foreach($kategoriSampah as $item)
                                            <option value="{{$item->id}}">{{$item->nama}}</option>
                                            @endforeach
                                        </select>
                                    </fieldset>
                                    <div class="form-group">
                                        <div class="controls">
                                            <label>Jumlah Sampah Masuk</label>
                                            <input type="number" class="form-control" placeholder="Jumlah Sampah Masuk (Kg)" name="jumlah_sampah" value="{{old('jumlah_sampah')}}" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 d-flex flex-sm-row flex-column justify-content-end mt-1">
                                    <a href="{{ route('manajemen_sampah') }}" class="btn btn-light mr-1">Cancel</a>
                                    <button type="submit" class="btn btn-primary glow mb-1 mb-sm-0 mr-0 mr-sm-1">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </section>
            <!-- users edit ends -->

        </div>
    </div>
</div>
<!-- END: Content-->
@endsection
@section('script')
<script>
    function getCategories() {
        var spinHandle = loadingOverlay.activate();
        var value = $("#titikPenampungan").val();
        $.ajax({
            url: "{{ url('dashboard/titik_penampungan_details/category') }}/"+value,
            type: 'get',
            dataType: 'json',
            success: function (res) {
                $('#kategoriSampah').empty();
                var option ='<option value="">Pilih Kategori Sampah</option>';
                $.each(res, function(key, obj) {
                    option += "<option value='" + obj.id_kategori + "'>"+obj.nama+"</option>";
                });
                $('#kategoriSampah').append(option);
                $('#kategoriSampah').trigger("chosen:updated");
                loadingOverlay.cancel(spinHandle);
            },
            error: function (xhr, textStatus, error) {
                $('#kategoriSampah').empty();
                $('#kategoriSampah').append('<option value="">Pilih Kategori Sampah</option>');
                loadingOverlay.cancel(spinHandle);
            }
        });
    }
</script>
@endsection