@extends('layouts.backendMainLayout')
@section('content')
<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
            <!-- users list start -->
            <section class="users-list-wrapper">
                <div class="users-list-filter px-1">
                    <form action="#" method="GET">
                        <div class="row border rounded py-2 mb-2">
                            <div class="col-12 col-sm-6 col-lg-11">
                                <label>Titik Penampungan</label>                                    
                                <fieldset class="form-group">
                                    <select class="form-control" id="titikPenampungan" name="qpenampungan" onChange="getCategories()">
                                        <option value="">Pilih Titik Penampungan</option>
                                        @foreach($titikPenampungan as $item)
                                        <option value="{{$item->id}}" <?php if(($qpenampungan!='')&&($item->id == $qpenampungan)) { echo 'selected'; } ?>>{{$item->nama}}</option>
                                        @endforeach
                                    </select>
                                </fieldset>
                                <label>Kategori Sampah</label>
                                <fieldset class="form-group">
                                    <select class="form-control" name="qkategori">
                                        <option value="">Pilih Kategori Sampah</option>
                                        @foreach($kategoriSampah as $item)
                                        <option value="{{$item->id}}" <?php if(($qkategori!='')&&($item->id == $qkategori)) { echo 'selected'; } ?>>{{$item->nama}}</option>
                                        @endforeach
                                    </select>
                                </fieldset>
                                <label>Tanggal</label>
                                <fieldset class="form-group position-relative has-icon-left">
                                    <input type="text" name="qtanggal" class="form-control pickadate" placeholder="Select Date" value="{{$qtanggal}}">
                                    <div class="form-control-position">
                                        <i class='bx bx-calendar'></i>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="col-12 col-sm-6 col-lg-1 d-flex align-items-center" style="padding:0px;">
                                <button type="submit" class="btn btn-icon rounded-circle btn-outline-primary mr-1"><i class="bx bx-search"></i></button>
                                <a href="{{route('manajemen_sampah')}}" class="btn btn-icon rounded-circle btn-outline-primary mr-1"><i class="bx bx-trash"></i></a>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="row" id="table-hover-row">
                    <div class="col-12">
                        <div class="card" style="min-height:400px">
                            <div class="card-header">
                                <h4 class="card-title">{{$title}}</h4>
                                <div class="d-flex align-items-center">
                                    <a href="{{route('manajemen_sampah_create')}}"><button type="button" class="btn btn-icon rounded-circle btn-outline-primary"><i class="bx bx-plus"></i></button></a>
                                </div>
                            </div>
                            <div class="card-body">
                                @if ($message = Session::get('success'))
                                    <div class="alert alert-success alert-dismissible mb-2" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <div class="d-flex align-items-center">
                                            <i class="bx bx-like"></i>
                                            <span>
                                                {{$message}}
                                            </span>
                                        </div>
                                    </div>
                                @endif
                                <div class="table-responsive">
                                    <table class="table table-hover mb-0">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Titik Penampungan</th>
                                                <th>Kategori Sampah</th>
                                                <th>Jumlah Sampah (Kg)</th>
                                                <th>Tanggal</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if(isset($results))
                                                <?php $no = 1; ?>
                                                @foreach($results as $item)
                                                <tr>
                                                    <td>{{$no++}}</td>
                                                    <td>{{$item->titik_penampungan->nama}}</td>
                                                    <td>{{$item->kategori_sampah->nama}}</td>
                                                    <td>{{$item->jumlah_sampah}}</td>
                                                    <td>{{$item->created_at->format('m/d/Y')}}</td>
                                                </tr>
                                                @endforeach
                                            @else
                                            <tr>
                                                <td colspan="6" style="text-align: center;">No Records</td>
                                            </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    @if(isset($results))
                    <div class="justify-content-end">
                        {!! $results->links() !!}
                    </div>
                    @endif
                </div>
            </section>
            <!-- users list ends -->
        </div>
    </div>
</div>
<!-- END: Content-->
@endsection