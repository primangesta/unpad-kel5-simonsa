<table border="1">
	<thead>
		<tr>
			<td>No</td>
			<td>Titik Penampungan</td>
			<td>Kategori Sampah</td>
			<td>Jumlah Sampah (Kg)</td>
			<td>Tanggal</td>
		</tr>
	</thead>
	<tbody>
		@php $no=1; @endphp
		@foreach($data as $val)
			<tr>
				<td>{{ $no }}</td>
				<td>{{ $val->titik_penampungan->nama}}</td>
				<td>{{ $val->kategori_sampah->nama}}</td>
				<td>{{ $val->jumlah_sampah}}</td>
				<td>{{ $val->created_at}}</td>
			</tr>
			@php $no++; @endphp
		@endforeach
	</tbody>
</table>