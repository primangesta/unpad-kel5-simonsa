@extends('layouts.backendMainLayout')
@section('content')
<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
            <!-- users list start -->
            <section class="users-list-wrapper">
                <div class="users-list-filter px-1">
                    <form action="#" method="GET">
                        <div class="row border rounded py-2 mb-2">
                            <div class="col-12 col-sm-6 col-lg-11">
                                <label for="users-list-verified">Filter</label>                                 
                                <fieldset class="form-group">
                                    <select class="form-control" name="q">
                                        <option value="">Pilih Kategori Sampah</option>
                                        @foreach($kategoriSampah as $item)
                                        <option value="{{$item->id}}" <?php if(($search_category!='')&&($item->id == $search_category)) { echo 'selected'; } ?>>{{$item->nama}}</option>
                                        @endforeach
                                    </select>
                                </fieldset>
                            </div>
                            <div class="col-12 col-sm-6 col-lg-1 d-flex align-items-center" style="padding:0px;">
                                <button type="submit" class="btn btn-icon rounded-circle btn-outline-primary mr-1"><i class="bx bx-search"></i></button>
                                <a href="{{route('titik_penampungan_details',$id_titik_penampungan)}}" class="btn btn-icon rounded-circle btn-outline-primary mr-1"><i class="bx bx-trash"></i></a>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="row" id="table-hover-row">
                    <div class="col-12">
                        <div class="card" style="min-height:400px">
                            <div class="card-header">
                                <h4 class="card-title">{{$title}} di {{$titikPenampungan->nama}}</h4>
                                <div class="d-flex align-items-center">
                                    <a href="{{route('titik_penampungan_details_create',$id_titik_penampungan)}}"><button type="button" class="btn btn-icon rounded-circle btn-outline-primary"><i class="bx bx-plus"></i></button></a>
                                </div>
                            </div>
                            <div class="card-body">
                            @if ($message = Session::get('success'))
                                <div class="alert alert-success alert-dismissible mb-2" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <div class="d-flex align-items-center">
                                        <i class="bx bx-like"></i>
                                        <span>
                                            {{$message}}
                                        </span>
                                    </div>
                                </div>
                            @endif
                            <div class="table-responsive">
                                <table class="table table-hover mb-0">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Kategori Sampah</th>
                                            <th>Kapasitas Tong</th>
                                            <th>Total Sampah</th>
                                            <th>Tanggal Terakhir Pembersihan</th>
                                            <th style="min-width:150px">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(isset($results))
                                            <?php $no = 1; ?>
                                            @foreach($results as $item)
                                            <tr>
                                                <td>{{$no++}}</td>
                                                <td>{{$item->kategori_sampah->nama}}</td>
                                                <td>{{$item->kapasitas_tong}}</td>
                                                <td>{{$item->total_sampah}}</td>
                                                <td>{{$item->tgl_pembersihan_sampah}}</td>
                                                <td>
                                                    <form action="{{route('titik_penampungan_details_clean',[$id_titik_penampungan,$item->id_kategori_sampah])}}" method="POST" style="display:contents">
                                                        @csrf
                                                        <button type="submit" onclick="return confirm('Anda yakin akan membersihkan data total sampah ini?');" style="border: none; background: none;"><i class="badge-circle badge-circle-light-secondary bx bxs-eraser font-medium-1"></i></button>
                                                    </form>
                                                    <a href="{{route('titik_penampungan_details_edit',[$id_titik_penampungan,$item->id])}}" style="margin-bottom:20px"><i class="badge-circle badge-circle-light-secondary bx bx-pencil font-medium-1"></i></a>
                                                    <form action="{{route('titik_penampungan_details_delete',[$id_titik_penampungan,$item->id])}}" method="POST" style="display:contents">
                                                        @csrf
                                                        <button type="submit" onclick="return confirm('Anda yakin akan menghapus data ini?');" style="border: none; background: none;"><i class="badge-circle badge-circle-light-secondary bx bx-trash font-medium-1"></i></button>
                                                    </form>
                                                </td>
                                            </tr>
                                            @endforeach
                                        @else
                                        <tr>
                                            <td colspan="4" style="text-align: center;">No Records</td>
                                        </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                @if(isset($results))
                <div class="justify-content-end">
                    {!! $results->links() !!}
                </div>
                @endif

            </section>
            <!-- users list ends -->
        </div>
    </div>
</div>
<!-- END: Content-->
@endsection