@extends('layouts.backendMainLayout')
@section('content')
<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
            <!-- users edit start -->
            <section class="users-edit">
                <div class="card">
                    <div class="card-body">
                        
                        @if (count($errors) > 0)
                            @foreach ($errors->all() as $error)
                                <div class="alert alert-danger alert-dismissible mb-2" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <div class="d-flex align-items-center">
                                        <i class="bx bx-error"></i>
                                        <span>
                                            {{$error}}
                                        </span>
                                    </div>
                                </div>
                            @endforeach
                        @endif

                        <form class="form-validate" action="{{ route('titik_penampungan_details_update',[$data->id_titik_penampungan,$data->id]) }}" method="POST">
                            @csrf
                            <div class="row">
                                
                                <div class="col-12 col-sm-12">
                                    <label>Kategori Sampah</label>                                    
                                    <fieldset class="form-group">
                                        <select class="form-control" name="id_kategori_sampah">
                                            <option value="">Pilih Kategori Sampah</option>
                                            @foreach($kategoriSampah as $item)
                                            <option value="{{$item->id}}" <?php if($item->id == $data->id_kategori_sampah) echo'selected'; ?>>{{$item->nama}}</option>
                                            @endforeach
                                        </select>
                                    </fieldset>
                                    <div class="form-group">
                                        <div class="controls">
                                            <label>Kapasitas Tong</label>
                                            <input type="number" class="form-control" placeholder="Kapasitas Tong" name="kapasitas_tong" value="{{$data->kapasitas_tong}}" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 d-flex flex-sm-row flex-column justify-content-end mt-1">
                                    <a href="{{ route('titik_penampungan_details',$data->id_titik_penampungan) }}" class="btn btn-light mr-1">Cancel</a>
                                    <button type="submit" class="btn btn-primary glow mb-1 mb-sm-0 mr-0 mr-sm-1">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </section>
            <!-- users edit ends -->

        </div>
    </div>
</div>
<!-- END: Content-->
@endsection