<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'Auth\AuthController@showFormLogin')->name('login');
Route::post('login', 'Auth\AuthController@login');

Route::group(['middleware' => 'auth'], function () {
    Route::get('logout', 'Auth\AuthController@logout')->name('logout');
	Route::get('profile','Auth\AuthController@editProfile')->name('profile');
	Route::post('profile','Auth\AuthController@changeProfile')->name('change_profile');
	
	Route::get('getDataSampah','Backend\HomeController@getDataSampah')->name('getDataSampah');
	Route::get('getDataPenampungan','Backend\HomeController@getDataPenampungan')->name('getDataPenampungan');

	Route::prefix('dashboard')->group(function () {
		Route::get('/','Backend\HomeController@index');
		Route::get('/download','Backend\HomeController@getDataManajemenSampah')->name('download_manajemen_sampah');
		
		Route::get('kategori_sampah','Backend\KategoriSampahController@index')->name('kategori_sampah');
		Route::get('kategori_sampah/create','Backend\KategoriSampahController@create')->name('kategori_sampah_create');
		Route::post('kategori_sampah','Backend\KategoriSampahController@store')->name('kategori_sampah_store');
		Route::get('kategori_sampah/{id}/edit','Backend\KategoriSampahController@edit')->name('kategori_sampah_edit');
		Route::post('kategori_sampah/{id}/edit','Backend\KategoriSampahController@update')->name('kategori_sampah_update');
		Route::post('kategori_sampah/{id}/delete','Backend\KategoriSampahController@destroy')->name('kategori_sampah_delete');
	
		Route::get('titik_penampungan','Backend\TitikPenampunganController@index')->name('titik_penampungan');
		Route::get('titik_penampungan/create','Backend\TitikPenampunganController@create')->name('titik_penampungan_create');
		Route::post('titik_penampungan','Backend\TitikPenampunganController@store')->name('titik_penampungan_store');
		Route::get('titik_penampungan/{id}/edit','Backend\TitikPenampunganController@edit')->name('titik_penampungan_edit');
		Route::post('titik_penampungan/{id}/edit','Backend\TitikPenampunganController@update')->name('titik_penampungan_update');
		Route::post('titik_penampungan/{id}/delete','Backend\TitikPenampunganController@destroy')->name('titik_penampungan_delete');
	
		Route::get('titik_penampungan_details/category/{id_titik_penampungan}','Backend\TitikPenampunganDetailsController@getCategories');
		Route::get('titik_penampungan_details/{id_titik_penampungan}','Backend\TitikPenampunganDetailsController@index')->name('titik_penampungan_details');
		Route::get('titik_penampungan_details/{id_titik_penampungan}/create','Backend\TitikPenampunganDetailsController@create')->name('titik_penampungan_details_create');
		Route::post('titik_penampungan_details/{id_titik_penampungan}','Backend\TitikPenampunganDetailsController@store')->name('titik_penampungan_details_store');
		Route::get('titik_penampungan_details/{id_titik_penampungan}/{id}/edit','Backend\TitikPenampunganDetailsController@edit')->name('titik_penampungan_details_edit');
		Route::post('titik_penampungan_details/{id_titik_penampungan}/{id}/edit','Backend\TitikPenampunganDetailsController@update')->name('titik_penampungan_details_update');
		Route::post('titik_penampungan_details/{id_titik_penampungan}/{id}/delete','Backend\TitikPenampunganDetailsController@destroy')->name('titik_penampungan_details_delete');
		Route::post('titik_penampungan_details/{id_titik_penampungan}/{id_kategori_sampah}/clean','Backend\TitikPenampunganDetailsController@cleanSampah')->name('titik_penampungan_details_clean');
	
		Route::get('manajemen_sampah','Backend\ManajemenSampahController@index')->name('manajemen_sampah');
		Route::get('manajemen_sampah/create','Backend\ManajemenSampahController@create')->name('manajemen_sampah_create');
		Route::post('manajemen_sampah','Backend\ManajemenSampahController@store')->name('manajemen_sampah_store');
		Route::get('manajemen_sampah/{id}/edit','Backend\ManajemenSampahController@edit')->name('manajemen_sampah_edit');
		Route::post('manajemen_sampah/{id}/edit','Backend\ManajemenSampahController@update')->name('manajemen_sampah_update');
		Route::post('manajemen_sampah/{id}/delete','Backend\ManajemenSampahController@destroy')->name('manajemen_sampah_delete');
	
		Route::get('users','Backend\UsersController@index')->name('user');
		Route::get('users/create','Backend\UsersController@create')->name('user_create');
		Route::post('users','Backend\UsersController@store')->name('user_store');
		Route::get('users/{id}/edit','Backend\UsersController@edit')->name('user_edit');
		Route::post('users/{id}/edit','Backend\UsersController@update')->name('user_update');
		Route::post('users/{id}/delete','Backend\UsersController@destroy')->name('user_delete');
	});
});