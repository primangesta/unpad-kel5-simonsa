<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use DB;
use Hash;
use Illuminate\Support\Arr;

class UsersController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request)
	{
		$data = User::select('*');

		if($request->q != ''){
			$data = $data->where('nama','ilike','%'.$request->q.'%')
						->orWhere('email','ilike','%'.$request->q.'%');
		}

		$data = $data->paginate(10);

		$results = $data;

		return view("backend.users.index", compact('results'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		return view('backend.users.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$this->validate($request, [
			'nama' => 'required',
			'email' => 'required|email|unique:users,email',
			'password' => 'required|same:confirm-password'
		]);

		$data = new User;
		$data->nama = $request->nama;
		$data->email = $request->email;
		$data->password = Hash::make($request->password);
		$data->save();
    	if($data){
			return redirect()->route('user')->with('success','Data User sukses ditambah');
		}else{
			return redirect()->route('user_create')->with('error','Data User gagal ditambah');
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		$data = User::find($id);
		if($data){
			return view("backend.users.edit")
					->with('data', $data);
		}else{
			return view("backend.users.index")->with('error','Data User tidak ditemukan');
		}
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$this->validate($request, [
			'nama' => 'required'
		]);

		$data = User::find($id);
		if($data){
			$data->nama	= $request->nama;
			$data->save();

			if($data){
				return redirect()->route('user')->with('success','Data User sukses diupdate');
			}else{
				return redirect()->route('user_create')->with('error','Data User gagal diupdate');
			}
		}else{
			return view("backend.user.index")->with('error','Data User tidak ditemukan');
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		$data = User::find($id);
		if($data){
			$data->delete();

			if($data){
				return redirect()->route('user')->with('success','Data User sukses dihapus');
			}else{
				return redirect()->route('user')->with('error','Data User gagal dihapus');
			}
		}else{
			return view("backend.users.index")->with('error','Data User tidak ditemukan');
		}
	}
}
