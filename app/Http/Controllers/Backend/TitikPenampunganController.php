<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\TitikPenampungan;

class TitikPenampunganController extends Controller
{
	public function index(Request $request){
		$data = TitikPenampungan::select('*');

		if($request->q != ''){
			$data = $data->where('nama','ilike','%'.$request->q.'%')
						->orWhere('deskripsi','ilike','%'.$request->q.'%');
		}

		$data = $data->paginate(10);

		$results = $data;

		return view("backend.titik_penampungan.index", compact('results'));
	}

	public function create(Request $request){
		return view("backend.titik_penampungan.create");
	}

	public function store(Request $request){
		$this->validate($request, [
			'nama' => 'required',
			'kapasitas_organik' 	=> 'required',
			'kapasitas_non_organik' => 'required'
		]);

		$data = new TitikPenampungan;
		$data->nama 				= $request->nama;
		$data->deskripsi 			= $request->deskripsi;
		$data->kapasitas_organik 	= $request->kapasitas_organik;
		$data->kapasitas_non_organik= $request->kapasitas_non_organik;
		$data->save();

		if($data){
			return redirect()->route('titik_penampungan')->with('success','Data Titik Penampungan sukses ditambah');
		}else{
			return redirect()->route('titik_penampungan_create')->with('error','Data Titik Penampungan gagal ditambah');
		}
	}

	public function edit(Request $request, $id){
		$data = TitikPenampungan::find($id);
		if($data){
			return view("backend.titik_penampungan.edit")
					->with('data', $data);
		}else{
			return view("backend.titik_penampungan.index")->with('error','Data Titik Penampungan tidak ditemukan');
		}
	}

	public function update(Request $request, $id){
		$this->validate($request, [
			'nama' => 'required',
			'kapasitas_organik' 	=> 'required',
			'kapasitas_non_organik' => 'required'
		]);

		$data = TitikPenampungan::find($id);
		if($data){
			$data->nama 		= $request->nama;
			$data->deskripsi 	= $request->deskripsi;
			$data->kapasitas_organik 	= $request->kapasitas_organik;
			$data->kapasitas_non_organik= $request->kapasitas_non_organik;
			$data->save();

			if($data){
				return redirect()->route('titik_penampungan')->with('success','Data Titik Penampungan sukses diupdate');
			}else{
				return redirect()->route('titik_penampungan_create')->with('error','Data Titik Penampungan gagal diupdate');
			}
		}else{
			return view("backend.titik_penampungan.index")->with('error','Data Titik Penampungan tidak ditemukan');
		}
	}

	public function destroy(Request $request, $id){
		$data = TitikPenampungan::find($id);
		if($data){
			$data->delete();

			if($data){
				return redirect()->route('titik_penampungan')->with('success','Data Titik Penampungan sukses dihapus');
			}else{
				return redirect()->route('titik_penampungan')->with('error','Data Titik Penampungan gagal dihapus');
			}
		}else{
			return view("backend.titik_penampungan.index")->with('error','Data Titik Penampungan tidak ditemukan');
		}
	}
}