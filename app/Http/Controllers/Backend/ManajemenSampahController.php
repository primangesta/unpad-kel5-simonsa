<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\ManajemenSampah;
use App\Models\TitikPenampungan;
use App\Models\TitikPenampunganDetails;
use App\Models\KategoriSampah;

class ManajemenSampahController extends Controller
{
	public function index(Request $request){
		$titikPenampungan 	= TitikPenampungan::select('id','nama')->where('is_active',true)->orderBy('nama','asc')->get();
		$kategoriSampah 	= KategoriSampah::select('id','nama')->where('is_active',true)->orderBy('nama','asc')->get();
		$data = ManajemenSampah::select('*');

		$qkategori = "";
		$qpenampungan = "";
		$qtanggal = "";

		if($request->qpenampungan != ''){
			$data = $data->where('id_titik_penampungan',$request->qpenampungan);
			$qpenampungan = $request->qpenampungan;
		}

		if($request->qkategori != ''){
			$data = $data->where('id_kategori_sampah',$request->qkategori);
			$qkategori = $request->qkategori;
		}

		if($request->qtanggal != ''){
			$data = $data->whereDate('created_at',$request->qtanggal);
			$time = strtotime($request->qtanggal);
			$newformat = date('d F, Y',$time);
			$qtanggal = $newformat;
		}

		$data = $data->with(['titik_penampungan','kategori_sampah'])->paginate(10);

		$results = $data;

		return view("backend.manajemen_sampah.index", compact('results'))
				->with('titikPenampungan',$titikPenampungan)
				->with('kategoriSampah',$kategoriSampah)
				->with('qkategori',$qkategori)
				->with('qpenampungan',$qpenampungan)
				->with('qtanggal',$qtanggal);
	}

	public function create(Request $request){
		$titikPenampungan 	= TitikPenampungan::select('id','nama')->where('is_active',true)->orderBy('nama','asc')->get();
		$kategoriSampah 	= KategoriSampah::select('id','nama')->where('is_active',true)->orderBy('nama','asc')->get();

		return view("backend.manajemen_sampah.create")
				->with('titikPenampungan',$titikPenampungan)
				->with('kategoriSampah',$kategoriSampah);
	}

	public function store(Request $request){
		$this->validate($request, [
			'id_titik_penampungan' => 'required',
			'id_kategori_sampah' => 'required',
			'jumlah_sampah' => 'required'
		]);

		$data = new ManajemenSampah;
		$data->id_titik_penampungan	= $request->id_titik_penampungan;
		$data->id_kategori_sampah	= $request->id_kategori_sampah;
		$data->jumlah_sampah		= $request->jumlah_sampah;
		$data->save();

		if($data){
			$updateTitikPenampungan = TitikPenampunganDetails::where('id_titik_penampungan',$request->id_titik_penampungan)
															->where('id_kategori_sampah',$request->id_kategori_sampah)
															->first();
			if($updateTitikPenampungan){
				$updateTitikPenampungan->total_sampah = $updateTitikPenampungan->total_sampah + $request->jumlah_sampah;
				$updateTitikPenampungan->save();
			}
			return redirect()->route('manajemen_sampah')->with('success','Data Manajemen Sampah sukses ditambah');
		}else{
			return redirect()->route('manajemen_sampah_create')->with('error','Data Manajemen Sampah gagal ditambah');
		}
	}
}