<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\TitikPenampunganDetails;
use App\Models\TitikPenampungan;
use App\Models\KategoriSampah;

class TitikPenampunganDetailsController extends Controller
{
	public function getCategories(Request $request, $id_titik_penampungan){
		$data = TitikPenampunganDetails::select('titik_penampungan_details.*','kategori_sampah.id as id_kategori','kategori_sampah.nama')
									->where('id_titik_penampungan',$id_titik_penampungan)
									->leftJoin('kategori_sampah','kategori_sampah.id','titik_penampungan_details.id_kategori_sampah')
									->get();

		return $data;
	}

	public function index(Request $request, $id_titik_penampungan){
		$titikPenampungan = TitikPenampungan::find($id_titik_penampungan);
		$kategoriSampah = KategoriSampah::select('id','nama')->where('is_active',true)->orderBy('nama','asc')->get();
		$data = TitikPenampunganDetails::select('*')->where('id_titik_penampungan',$id_titik_penampungan);

		$search_category = "";
		if($request->q != ''){
			$search_category = $request->q;
			$data = $data->where('id_kategori_sampah',$request->q);
		}

		$data = $data->with('kategori_sampah')->paginate(10);

		$results = $data;

		return view("backend.titik_penampungan_details.index", compact('results'))
				->with('id_titik_penampungan',$id_titik_penampungan)
				->with('titikPenampungan', $titikPenampungan)
				->with('kategoriSampah',$kategoriSampah)
				->with('search_category',$search_category);
	}

	public function create(Request $request, $id_titik_penampungan){
		$kategoriSampah = KategoriSampah::select('id','nama')->where('is_active',true)->orderBy('nama','asc')->get();

		return view("backend.titik_penampungan_details.create")
				->with('id_titik_penampungan',$id_titik_penampungan)
				->with('kategoriSampah',$kategoriSampah);
	}

	public function store(Request $request, $id_titik_penampungan){
		$this->validate($request, [
			'id_kategori_sampah' => 'required',
			'kapasitas_tong' 	=> 'required'
		]);

		$checkDB = TitikPenampunganDetails::where('id_titik_penampungan',$id_titik_penampungan)
											->where('id_kategori_sampah',$request->id_kategori_sampah)
											->first();
		
		if($checkDB){
			return redirect()->route('titik_penampungan_details_create',$id_titik_penampungan)->with('error','Data untuk kategori ini sudah ada');
		}

		$data = new TitikPenampunganDetails;
		$data->id_titik_penampungan	= $request->id_titik_penampungan;
		$data->id_kategori_sampah	= $request->id_kategori_sampah;
		$data->kapasitas_tong		= $request->kapasitas_tong;
		$data->save();

		if($data){
			return redirect()->route('titik_penampungan_details',$id_titik_penampungan)->with('success','Data Titik Penampungan sukses ditambah');
		}else{
			return redirect()->route('titik_penampungan_details_create',$id_titik_penampungan)->with('error','Data Titik Penampungan gagal ditambah');
		}
	}

	public function edit(Request $request, $id){
		$kategoriSampah = KategoriSampah::select('id','nama')->where('is_active',true)->orderBy('nama','asc')->get();
		$data = TitikPenampunganDetails::find($id);
		if($data){
			return view("backend.titik_penampungan_details.edit")
					->with('data', $data)
					->with('kategoriSampah',$kategoriSampah);
		}else{
			return view("backend.titik_penampungan_details.index")->with('error','Data Titik Penampungan tidak ditemukan');
		}
	}

	public function update(Request $request, $id, $id_titik_penampungan){
		$this->validate($request, [
			'id_kategori_sampah' => 'required',
			'kapasitas_tong' 	=> 'required'
		]);

		$data = TitikPenampunganDetails::find($id);
		if($data){
			$data->id_titik_penampungan	= $request->id_titik_penampungan;
			$data->id_kategori_sampah	= $request->id_kategori_sampah;
			$data->kapasitas_tong		= $request->kapasitas_tong;
			$data->save();

			if($data){
				return redirect()->route('titik_penampungan_details',$id_titik_penampungan)->with('success','Data Titik Penampungan sukses diupdate');
			}else{
				return redirect()->route('titik_penampungan_details_create',$id_titik_penampungan)->with('error','Data Titik Penampungan gagal diupdate');
			}
		}else{
			return redirect()->route("titik_penampungan_details",$id_titik_penampungan)->with('error','Data Titik Penampungan tidak ditemukan');
		}
	}

	public function destroy(Request $request, $id, $id_titik_penampungan){
		$data = TitikPenampunganDetails::find($id);
		if($data){
			$data->delete();

			if($data){
				return redirect()->route('titik_penampungan_details',$id_titik_penampungan)->with('success','Data Titik Penampungan sukses dihapus');
			}else{
				return redirect()->route('titik_penampungan_details',$id_titik_penampungan)->with('error','Data Titik Penampungan gagal dihapus');
			}
		}else{
			return redirect()->route('titik_penampungan_details',$id_titik_penampungan)->with('error','Data Titik Penampungan tidak ditemukan');
		}
	}

	public function cleanSampah(Request $request, $id_titik_penampungan, $id_kategori_sampah){
		$data = TitikPenampunganDetails::where('id_titik_penampungan',$id_titik_penampungan)
														->where('id_kategori_sampah',$id_kategori_sampah)
														->first();
		if($data){
			$data->total_sampah = 0;
			$data->tgl_pembersihan_sampah = date('Y-m-d');
			$data->save();

			if($data){
				return redirect()->route('titik_penampungan_details',$id_titik_penampungan)->with('success','Data Titik Penampungan sukses dibersihkan');
			}else{
				return redirect()->route('titik_penampungan_details',$id_titik_penampungan)->with('error','Data Titik Penampungan gagal dibersihkan');
			}
		}else{
			return redirect()->route('titik_penampungan_details',$id_titik_penampungan)->with('error','Data Titik Penampungan tidak ditemukan');
		}
	}
}