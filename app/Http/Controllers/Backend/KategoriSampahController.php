<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\KategoriSampah;

class KategoriSampahController extends Controller
{
	public function index(Request $request){
		$data = KategoriSampah::select('*');

		if($request->q != ''){
			$data = $data->where('nama','ilike','%'.$request->q.'%')
						->orWhere('deskripsi','ilike','%'.$request->q.'%');
		}

		$data = $data->paginate(10);

		$results = $data;

		return view("backend.kategori_sampah.index", compact('results'));
	}

	public function create(Request $request){
		return view("backend.kategori_sampah.create");
	}

	public function store(Request $request){
		$this->validate($request, [
			'nama' => 'required'
		]);

		$data = new KategoriSampah;
		$data->nama 		= $request->nama;
		$data->deskripsi 	= $request->deskripsi;
		$data->save();

		if($data){
			return redirect()->route('kategori_sampah')->with('success','Kategori Sampah sukses ditambah');
		}else{
			return redirect()->route('kategori_sampah_create')->with('error','Kategori Sampah gagal ditambah');
		}
	}

	public function edit(Request $request, $id){
		$data = KategoriSampah::find($id);
		if($data){
			return view("backend.kategori_sampah.edit")
					->with('data', $data);
		}else{
			return view("backend.kategori_sampah.index")->with('error','Kategori Sampah tidak ditemukan');
		}
	}

	public function update(Request $request, $id){
		$this->validate($request, [
			'nama' => 'required'
		]);

		$data = KategoriSampah::find($id);
		if($data){
			$data->nama 		= $request->nama;
			$data->deskripsi 	= $request->deskripsi;
			$data->save();

			if($data){
				return redirect()->route('kategori_sampah')->with('success','Data Kategori Sampah sukses diupdate');
			}else{
				return redirect()->route('kategori_sampah_create')->with('error','Data Kategori Sampah gagal diupdate');
			}
		}else{
			return view("backend.kategori_sampah.index")->with('error','Data Kategori Sampah tidak ditemukan');
		}
	}

	public function destroy(Request $request, $id){
		$data = KategoriSampah::find($id);
		if($data){
			$data->delete();

			if($data){
				return redirect()->route('kategori_sampah')->with('success','Data Kategori Sampah sukses dihapus');
			}else{
				return redirect()->route('kategori_sampah_create')->with('error','Data Kategori Sampah gagal dihapus');
			}
		}else{
			return view("backend.kategori_sampah.index")->with('error','Data Kategori Sampah tidak ditemukan');
		}
	}
}