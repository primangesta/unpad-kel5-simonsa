<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\ManajemenSampahExport;

use App\Models\KategoriSampah;
use App\Models\ManajemenSampah;
use App\Models\TitikPenampungan;
use App\Models\TitikPenampunganDetails;

class HomeController extends Controller
{
	public function index(Request $request){
		$titikPenampungan 	= TitikPenampungan::all();
		$kategoriSampah 	= KategoriSampah::all();

		$data = ManajemenSampah::select('*')->where('id_kategori_sampah',2)->sum('jumlah_sampah'); // Jenis Non Organik
		$total = $data * 200; // Nilai terendah (kantong plastik)

		$result = [];
		foreach($titikPenampungan as $tp){
			foreach($kategoriSampah as $ks){
				$arr = [];
				$data = TitikPenampunganDetails::where('id_titik_penampungan',$tp->id)
												->where('id_kategori_sampah',$ks->id)
												->first();

				$arr['code'] = $tp->id.'-'.$ks->id;
				$arr['nama'] = $tp->nama;
				$arr['kategori'] = $ks->nama;
				$arr['total'] = round(($data->total_sampah / $data->kapasitas_tong)*100);

				array_push($result,$arr);
			}
		}

		return view("backend.index")
				->with('data',$total)
				->with('result',$result);

	}

	public function getDataSampah(Request $request){
		$kategoriSampah = KategoriSampah::select('id','nama')->where('is_active',true)->orderBy('nama','asc')->get();

		$result = [];
		foreach($kategoriSampah as $item){
			$mSampah = ManajemenSampah::select('id', 'id_kategori_sampah', 'jumlah_sampah','created_at')
									->where('id_kategori_sampah',$item->id)
									->get()
									->groupBy(function($date) {
										return Carbon::parse($date->created_at)->format('m'); // grouping by months
									});
									
			$sampahCount = [];
			$arr 	= [];
			foreach ($mSampah as $key => $value) {
				$total 	= 0;
				foreach($value as $idx){
					$total = $total + $idx->jumlah_sampah;
				}

				$sampahCount[(int)$key] = $total;
			}

			for($i = 1; $i <= 12; $i++){
				if(!empty($sampahCount[$i])){
					// $arr[$i] = $sampahCount[$i];
					array_push($arr, $sampahCount[$i]);  
				}else{
					// $arr[$i] = 0; 
					array_push($arr, 0);   
				}
			}
			$temp = [];
			$temp['name'] = $item->nama;
			$temp['data'] = $arr;
			array_push($result,$temp);
		}

		return $result;
	}

	public function getDataPenampungan(Request $request){
		$titikPenampungan 	= TitikPenampungan::all();
		$kategoriSampah 	= KategoriSampah::all();

		$result = [];
		foreach($titikPenampungan as $tp){
			foreach($kategoriSampah as $ks){
				$arr = [];
				$data = TitikPenampunganDetails::where('id_titik_penampungan',$tp->id)
												->where('id_kategori_sampah',$ks->id)
												->first();

				$arr['code'] = $tp->id.'-'.$ks->id;
				$arr['nama'] = $tp->nama;
				$arr['kategori'] = str_replace("Sampah ","",$ks->nama);
				$arr['total'] = round(($data->total_sampah / $data->kapasitas_tong)*100);

				array_push($result,$arr);
			}
		}

		return $result;
	}

	public function getDataManajemenSampah(Request $request){
		$data = ManajemenSampah::select('*')
							->orderBy('created_at')
							->with(['titik_penampungan','kategori_sampah'])
							->get();

		return Excel::download(new ManajemenSampahExport($data), 'data-manajemen-sampah-'.date('Y-m-d').'.xlsx');
	}
}