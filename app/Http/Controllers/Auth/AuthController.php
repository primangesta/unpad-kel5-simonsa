<?php
 
namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use Hash;
use Session;
use App\Models\User;
 
 
class AuthController extends Controller
{
    public function showFormLogin()
    {
        if (Auth::check()) { // true sekalian session field di users nanti bisa dipanggil via Auth
            //Login Success
            return redirect('dashboard');
        }
        return view('auth.login');
    }
 
    public function login(Request $request)
    {
        $rules = [
            'email'                 => 'required|email',
            'password'              => 'required|string'
        ];
 
        $messages = [
            'email.required'        => 'Email wajib diisi',
            'email.email'           => 'Email tidak valid',
            'password.required'     => 'Password wajib diisi',
            'password.string'       => 'Password harus berupa string'
        ];
 
        $validator = Validator::make($request->all(), $rules, $messages);
 
        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput($request->all);
        }
 
        $data = [
            'email'     => $request->input('email'),
            'password'  => $request->input('password'),
        ];
 
        Auth::attempt($data);
 
        if (Auth::check()) {
            //Login Success
            return redirect('dashboard');
        } else { // false
            //Login Fail
            Session::flash('error', 'Email atau password salah');
            return redirect()->route('login');
        }
 
    }
 
    public function logout()
    {
        Auth::logout(); // menghapus session yang aktif
        return redirect('/');
    }

    public function editProfile(){
        $data = Auth::user();
        return view('auth.editProfile')
                ->with('data',$data);
    }

    public function changeProfile(Request $request){
        $this->validate($request, [
			'nama' => 'required',
			'password' => 'required|same:confirm_password'
		]);

        $user = Auth::user();
        $data = User::find($user->id);
        if($data){
            if(Hash::check($request->current_password, $data->password)) { 
                $data->nama     = $request->nama;
                $data->password = Hash::make($request->password);
                $data->save();

                if($data){
                    return redirect()->route('profile')->with('success','Data Profile sukses diupdate');
                }else{
                    return redirect()->route('profile')->with('error','Data Profile gagal diupdate');
                }
            }else{
                return redirect()->route('profile')->with('error','Masukkan password lama dengan benar');
            }
        }else{
            return redirect()->route('profile')->with('error','Data user tidak ditemukan');
        }
    }
 
 
}
