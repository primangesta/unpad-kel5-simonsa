<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\ManajemenSampah;
use App\Models\TitikPenampungan;
use App\Models\TitikPenampunganDetails;
use App\Models\KategoriSampah;

class APIController extends Controller
{
	public function indexKategori(Request $request){
		$data = KategoriSampah::select('*');

        if($request->has('search')){
            $data = $data->where('nama','ilike','%'.$request->q.'%')
						->orWhere('deskripsi','ilike','%'.$request->q.'%');
        }

        $total = $data->count();
    
        if($request->has('limit')){
            $data->take($request->get('limit'));
            
            if($request->has('offset')){
            	$data->skip($request->get('offset'));
            }
        }

        if($request->has('order_type')){
            if($request->get('order_type') == 'asc'){
                if($request->has('order_by')){
                    $data->orderBy($request->get('order_by'));
                }else{
                    $data->orderBy('created_at');
                }
            }else{
                if($request->has('order_by')){
                    $data->orderBy($request->get('order_by'), 'desc');
                }else{
                    $data->orderBy('created_at', 'desc');
                }
            }
        }else{
            $data->orderBy('created_at', 'desc');
        }

        $data = $data->get();

        $response = [
            'status'    => true, 
            'message'   => 'Success',
            'total_row' => $total,
            'data'      => $data,
        ];

        return response()->json($response, 200);
	}

	public function indexPenampung(Request $request){
		$data = TitikPenampungan::select('*');

        if($request->has('search')){
            $data = $data->where('nama','ilike','%'.$request->q.'%')
						->orWhere('deskripsi','ilike','%'.$request->q.'%');
        }

        $total = $data->count();
    
        if($request->has('limit')){
            $data->take($request->get('limit'));
            
            if($request->has('offset')){
            	$data->skip($request->get('offset'));
            }
        }

        if($request->has('order_type')){
            if($request->get('order_type') == 'asc'){
                if($request->has('order_by')){
                    $data->orderBy($request->get('order_by'));
                }else{
                    $data->orderBy('created_at');
                }
            }else{
                if($request->has('order_by')){
                    $data->orderBy($request->get('order_by'), 'desc');
                }else{
                    $data->orderBy('created_at', 'desc');
                }
            }
        }else{
            $data->orderBy('created_at', 'desc');
        }

        $data = $data->get();

        $response = [
            'status'    => true, 
            'message'   => 'Success',
            'total_row' => $total,
            'data'      => $data,
        ];

        return response()->json($response, 200);
	}

	public function storeSampah(Request $request){
		$this->validate($request, [
			'id_titik_penampungan' => 'required',
			'id_kategori_sampah' => 'required',
			'jumlah_sampah' => 'required'
		]);

		$data = new ManajemenSampah;
		$data->id_titik_penampungan	= $request->id_titik_penampungan;
		$data->id_kategori_sampah	= $request->id_kategori_sampah;
		$data->jumlah_sampah		= $request->jumlah_sampah;
		$data->save();

		if($data){
			$updateTitikPenampungan = TitikPenampunganDetails::where('id_titik_penampungan',$request->id_titik_penampungan)
															->where('id_kategori_sampah',$request->id_kategori_sampah)
															->first();
			if($updateTitikPenampungan){
				$updateTitikPenampungan->total_sampah = $updateTitikPenampungan->total_sampah + $request->jumlah_sampah;
				$updateTitikPenampungan->save();
			}

			$response = [
				'status'    => true, 
				'message'   => 'Success'
			];
	
			return response()->json($response, 200);
		}else{
			$response = [
				'status'    => false, 
				'message'   => 'Gagal'
			];
	
			return response()->json($response, 200);
		}
	}

	public function cleanSampah(Request $request){
		$this->validate($request, [
			'id_titik_penampungan' => 'required',
			'id_kategori_sampah' => 'required',
			'tanggal' => 'required'
		]);

		$data = TitikPenampunganDetails::where('id_titik_penampungan',$request->id_titik_penampungan)
														->where('id_kategori_sampah',$request->id_kategori_sampah)
														->first();
		if($data){
			$data->total_sampah = 0;
			$data->tgl_pembersihan_sampah = $request->tanggal;
			$data->save();

			if($data){
				$response = [
					'status'    => true, 
					'message'   => 'Success'
				];
		
				return response()->json($response, 200);
			}else{
				$response = [
					'status'    => false, 
					'message'   => 'Gagal'
				];
		
				return response()->json($response, 200);
			}
		}else{
			$response = [
				'status'    => false, 
				'message'   => 'Data not found'
			];
	
			return response()->json($response, 200);
		}
	}
}