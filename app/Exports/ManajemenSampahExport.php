<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\FromView;


class ManajemenSampahExport implements FromView, ShouldQueue
{
	private $data;

	public function __construct($data)
	{
		$this->data = $data;
	}

	public function view(): View
	{
		return view('backend/export/manajemen_sampah', [
			'data' => $this->data
		]);
	}
}
