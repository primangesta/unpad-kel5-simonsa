<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ManajemenSampah extends Model
{
    use SoftDeletes;
    protected $table = 'manajemen_sampah';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nama',
        'id_titik_penampungan',
        'id_kategori_sampah',
        'jumlah_sampah'
    ];

    function titik_penampungan(){
		return $this->belongsTo(TitikPenampungan::class,'id_titik_penampungan','id');
	}

    function kategori_sampah(){
		return $this->belongsTo(KategoriSampah::class,'id_kategori_sampah','id');
	}
}
