<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TitikPenampunganDetails extends Model
{
    use SoftDeletes;
    protected $table = 'titik_penampungan_details';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_titik_penampungan',
        'id_kategori_sampah',
        'kapasitas_tong',
        'total_sampah',
        'tgl_pembersihan_sampah'
    ];

    function kategori_sampah(){
		return $this->belongsTo(KategoriSampah::class,'id_kategori_sampah','id');
	}
}
