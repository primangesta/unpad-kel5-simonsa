<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Request;

class MenuServiceProvider extends ServiceProvider
{
	/**
	 * Register services.
	 *
	 * @return void
	 */
	public function register()
	{
		//
	}

	/**
	 * Bootstrap services.
	 *
	 * @return void
	 */
	public function boot()
	{
		$page 	= "";
		$title 	= "";

		if(Request::segment(1) == 'dashboard'){
			if(Request::segment(2) == 'titik_penampungan'){
				$page 	= "titik_penampungan";
				$title 	= "Titik Penampungan";
			}elseif(Request::segment(2) == 'kategori_sampah'){
				$page 	= "kategori_sampah";
				$title 	= "Kategori Sampah";
			}elseif(Request::segment(2) == 'manajemen_sampah'){
				$page 	= "manajemen_sampah";
				$title 	= "Manajemen Sampah";
			}elseif(Request::segment(2) == 'titik_penampungan_details'){
				$page 	= "titik_penampungan_details";
				$title 	= "Titik Penampungan";
			}elseif(Request::segment(2) == 'users'){
				$page 	= "users";
				$title 	= "Users";
			}else{
				$page 	= "home";
				$title 	= "Home";
			}
		}

		view()->share('page', $page);
		view()->share('title', $title);
	}
}
