<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
	        'nama'	=> 'Super Admin',
	        'email'	=> 'superadmin@admin.com',
	        'password'	=> Hash::make('admin')
		]);
    }
}
