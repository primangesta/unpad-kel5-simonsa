<?php

use Illuminate\Database\Seeder;
use App\Models\KategoriSampah;

class KategoriSampahSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = KategoriSampah::create([
	        'nama'	=> 'Sampah Organik',
	        'deskripsi'	=> 'Sampah yang berasal dari sisa mahkluk hidup yang mudah terurai secara alami tanpa proses campur tangan manusia untuk dapat terurai'
		]);

        $data = KategoriSampah::create([
	        'nama'	=> 'Sampah Non Organik',
	        'deskripsi'	=> 'Sampah yang sudah tidak dipakai lagi dan sulit terurai'
		]);
    }
}
