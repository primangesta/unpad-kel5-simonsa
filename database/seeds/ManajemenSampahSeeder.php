<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\Models\TitikPenampungan;
use App\Models\TitikPenampunganDetails;
use App\Models\KategoriSampah;
use App\Models\ManajemenSampah;

class ManajemenSampahSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $kategori = KategoriSampah::all();
        for($i = 0; $i<5; $i++){
            foreach($kategori as $kat){
                $titikPenampungan = TitikPenampungan::all();
                foreach($titikPenampungan as $tp){
                    $random = rand(10,70);
                    $backwardDays = -300;
                    $backwardCreatedDays = rand($backwardDays, 0);
                    $date = Carbon::now()->addDays($backwardCreatedDays)->addMinutes(rand(0,
				60 * 23))->addSeconds(rand(0, 60));
                    $data = ManajemenSampah::create([
                        'id_titik_penampungan'	=> $tp->id,
                        'id_kategori_sampah'	=> $kat->id,
                        'jumlah_sampah'	        => $random,
                        'created_at'            => $date,
                        'updated_at'            => $date
                    ]);

                    $data = TitikPenampunganDetails::where('id_titik_penampungan', $tp->id)
                                                    ->where('id_kategori_sampah',$kat->id)
                                                    ->first();

                    if($data){
                        $data->total_sampah = $data->total_sampah + $random;
                        $data->save();
                    }
                }
            }
        }
    }
}
