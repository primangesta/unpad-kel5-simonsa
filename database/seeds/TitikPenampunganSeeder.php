<?php

use Illuminate\Database\Seeder;
use App\Models\TitikPenampungan;
use App\Models\TitikPenampunganDetails;
use App\Models\KategoriSampah;

class TitikPenampunganSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = TitikPenampungan::create([
	        'nama'	=> 'Gerbang Utama UNPAD',
	        'deskripsi'	=> 'Tempat sampah yang berada di Gerbang Utama UNPAD'
		]);

        $data = TitikPenampungan::create([
	        'nama'	=> 'Depan Perpustakaan UNPAD',
	        'deskripsi'	=> 'Tempat sampah yang berada di Depan Perpustakaan UNPAD'
		]);

        $kategori = KategoriSampah::all();
        foreach($kategori as $kat){
            $titikPenampungan = TitikPenampungan::all();
            foreach($titikPenampungan as $tp){
                $data = TitikPenampunganDetails::create([
                    'id_titik_penampungan'	=> $tp->id,
                    'id_kategori_sampah'	=> $kat->id,
                    'kapasitas_tong'	    => 300
                ]);
            }
        }
        
    }
}
