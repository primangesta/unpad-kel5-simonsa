<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTitikPenampunganDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('titik_penampungan_details', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('id_titik_penampungan');
            $table->bigInteger('id_kategori_sampah');
            $table->double('kapasitas_tong')->default(0);
            $table->double('total_sampah')->nullable();
            $table->date('tgl_pembersihan_sampah')->nullable();
			$table->timestamps();
            $table->softDeletes();

            $table->foreign('id_titik_penampungan')
					->references('id')->on('titik_penampungan')
					->onDelete('cascade');

			$table->foreign('id_kategori_sampah')
					->references('id')->on('kategori_sampah')
					->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('titik_penampungan_details');
    }
}
